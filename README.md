# chat_box 组件

Vue2 组件

## 功能：

左侧自定义区域，右侧聊天框；
通过传入数组参数渲染聊天框对话情景；
在聊天框右侧有更多选项按钮，在输入框下方调出自定义界面；
左侧区域完全自定义，也可以取消该区域，只保留右侧聊天框；
可以更改界面主题和按钮样式。

## 用法：

通过 npm 下载包

```
npm install bis_chat_box
```

通过 Vue.use()安装组件

```
import bis_chat_box from "bis_chat_box"
Vue.use(bis_chat_box)
```

## 属性：

### messageList 数组参数

messageList 是聊天数据数组，类型是一个对象数组，数组内的参数必须与下表匹配。

| 名称     | 类型           | 说明                                                                    |
| -------- | -------------- | ----------------------------------------------------------------------- |
| portrait | String         | 头像图片路径                                                            |
| text     | String         | 聊天文本                                                                |
| location | "left"/"right" | 对话显示位置                                                            |
| img      | Array          | 数组内容为 String，是对话图片路径，数组长度建议小于等于 4，以免页面变形 |

### ChatBox

整合自定义区域和聊天框区域。

#### 参数

<table>
    <tr align=left>
        <th>名称</th>
        <th>是否必要</th>
        <th>类型</th>
        <th>默认值</th>
        <th>说明</th>    
    </tr>
    <tr>
        <td>messageList</td>
        <td>true</td>
        <td>Array</td>
        <td>/</td>
        <td>详见上一节</td>
    </tr>
    <tr>
        <td>theme</td>
        <td>false</td>
        <td>String</td>
        <td>"lightblue"</td>
        <td>设置主题background样式，需要与css background的内容格式相同</td>
    </tr>
    <tr>
        <td>btn</td>
        <td>false</td>
        <td>String</td>
        <td>"palegreen"</td>
        <td>设置按钮background样式，格式同上</td>
    </tr>
    <tr>
        <td>msgBox</td>
        <td>false</td>
        <td>String</td>
        <td>"white"</td>
        <td>设置聊天框background样式，格式同上</td>
    </tr>
    <tr>
        <td>leftSize</td>
        <td>false</td>
        <td>Number</td>
        <td>1</td>
        <td rowspan="2">左边自定义区域和右边聊天框的宽度比例。例：leftSize=2,rightSize=1,则左边自定义区域占整个工具的2/3，右边聊天框占1/3</td>
    </tr>
    <tr>
        <td>rightSize</td>
        <td>false</td>
        <td>Number</td>
        <td>1</td>
    </tr>
    <tr>
        <td>moreFun</td>
        <td>false</td>
        <td>Boolean</td>
        <td>true</td>
        <td>开启聊天框自定义选项功能</td>
    </tr>
</table>

#### 事件

| 名称      | 说明                                     | 回调参数                               |
| --------- | ---------------------------------------- | -------------------------------------- |
| clickImg  | 点击对话中的图片时触发                   | {index:对话数组下标,imgIndex:图片下标} |
| clickMsg  | 点击对话（图片外）区域时触发             | {index:对话数组下标,event:MouseEvent}  |
| clickMore | 点击更多选项按钮时触发                   | /                                      |
| sendMsg   | 点击发送按钮，或在输入框位置按回车时触发 | 发送的信息内容                         |

#### 插槽

| 名称      | 说明                     |
| --------- | ------------------------ |
| leftPanel | 左边自定义区域           |
| more      | 右边聊天框自定义设置区域 |

### MessageBox 标签

聊天栏，支持单独使用。

#### 参数

| 名称                   | 是否必要 | 类型    | 默认值      | 说明                                                           |
| ---------------------- | -------- | ------- | ----------- | -------------------------------------------------------------- |
| messageList            | true     | Array   | /           | 详见上一节                                                     |
| isIndependentComponent | false    | Boolean | true        | 是否单独使用该组件                                             |
| theme                  | false    | String  | "lightblue" | 设置主题 background 样式，需要与 css background 的内容格式相同 |
| btn                    | false    | String  | "palegreen" | 设置按钮 background 样式，格式同上                             |
| msgBox                 | false    | String  | "white"     | 设置聊天框 background 样式，格式同上                           |
| moreFun                | false    | Boolean | true        | 开启聊天框自定义选项功能                                       |

#### 事件

| 名称      | 说明                                     | 回调参数                               |
| --------- | ---------------------------------------- | -------------------------------------- |
| clickImg  | 点击对话中的图片时触发                   | {index:对话数组下标,imgIndex:图片下标} |
| clickMsg  | 点击对话（图片外）区域时触发             | {index:对话数组下标,event:MouseEvent}  |
| clickMore | 点击更多选项按钮时触发                   | /                                      |
| sendMsg   | 点击发送按钮，或在输入框位置按回车时触发 | 发送的信息内容                         |

#### 插槽

| 名称 | 说明                     |
| ---- | ------------------------ |
| more | 右边聊天框自定义设置区域 |

### SingleMessage 标签

单条对话标签

#### 参数

| 名称     | 是否必要 | 类型         | 默认值 | 说明         |
| -------- | -------- | ------------ | ------ | ------------ |
| portrait | true     | String       | /      | 头像图片路径 |
| text     | true     | String       | /      | 文本         |
| location | true     | "left/right" | /      | 对话显示位置 |
| img      | false    | Array        | /      | 图片路径数组 |
| index    | true     | Number       | /      | 对话序号     |

#### 事件

| 名称     | 说明                         | 回调参数                               |
| -------- | ---------------------------- | -------------------------------------- |
| clickImg | 点击对话中的图片时触发       | {index:对话数组下标,imgIndex:图片下标} |
| clickMsg | 点击对话（图片外）区域时触发 | {index:对话数组下标,event:MouseEvent}  |
