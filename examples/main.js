import Vue from "vue"
import App from "./App.vue"
import ChatBox from "../packages/index"

Vue.config.productionTip = false

Vue.use(ChatBox)

new Vue({
  render: (h) => h(App),
}).$mount("#app")
