import SingleMessage from "./SingleMessage"
import MessageBox from "./MessageBox"
import ChatBox from "./ChatBox"

const Components = [SingleMessage, MessageBox, ChatBox]

const install = function (Vue) {
  if (install.installed) return
  Components.map((component) => Vue.component(component.name, component))
}

if (typeof window !== "undefined" && window.Vue) {
  install(window.Vue)
}

export default {
  install,
  ...Components,
}
