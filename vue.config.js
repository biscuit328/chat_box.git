const { defineConfig } = require("@vue/cli-service")
const path = require("path")
function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  pages: {
    index: {
      entry: "examples/main.js",
      template: "public/index.html",
      filename: "index.html",
    },
  },
  css: {
    extract: false,
  },
  chainWebpack: (config) => {
    config.resolve.alias
      .set("@", resolve("examples"))
      .set("~", resolve("packages"))
  },
  transpileDependencies: true,
}
